# mariadb Build inspired by lando
FROM mariadb:latest

LABEL Maintainer="Lee Pang <lee@2ton.com>" \
      Description="MySQL container based on MariaDB."